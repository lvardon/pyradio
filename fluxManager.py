#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Simple Radio Stream Manager and player

Beta

Dec 2016

Licence GPL


"""
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.Qt import *
from PyQt5 import QtWidgets
import sys, os, subprocess, time, datetime, shutil, webbrowser, re
from subprocess import call

configCol0Play=0
configCol4Rec=1
configCol3Logo=2
configColxName=3
configCol5Genre=4
configColxDescription=5
configCol2Stream=6

debugMode=1

def printDebug(  message ) :
    #global debugMode
    #if int(self.settings.value("debug/configDebug", 0)) == 1 :
    #if debugMode == 1 :
    print(message)

# http://qt.developpez.com/faq/?page=modules-qtcore-fichiers-lectureecriture
# https://books.google.fr/books?id=9oRa4WJLlGkC&pg=PT212&lpg=PT212&dq=python+qt+++QDataStream+write&source=bl&ots=XoiWGsKxnl&sig=W5xhkgfMjzKI-p43CEKfa0Fc5bQ&hl=en&sa=X&redir_esc=y#v=onepage&q=python%20qt%20%20%20QDataStream%20write&f=false
class DatastreamIo() :

    def writeToFile(self, fileName, dataMyStreamList ) :

        printDebug("start write : " + fileName)

        file = QFile(fileName)
        file.open(QIODevice.WriteOnly)
        datastream = QDataStream(file)

        printDebug("start write streamList")
        datastream.writeQVariant( dataMyStreamList.streamList )
        printDebug("start write streamListFavorite")
        datastream.writeQVariant( dataMyStreamList.streamListFavorite )
        printDebug("start write streamListAdvert")
        datastream.writeQVariant( dataMyStreamList.streamListAdvert )

        #for streamItem  in dataMyStreamList.streamList :
            #printDebug("save url = " + streamItem.url + " genre=" + streamItem.getGenre() + ", name =" +  streamItem.getName()  )
            #for nowPlaying  in streamItem.nowPlaying :
            #   printDebug("nowPlaying = " + nowPlaying.getTrackItem() +", date = " + nowPlaying.getTrackItemDt()  )
        #for streamItem  in dataMyStreamList.streamListFavorite :
        #   printDebug("* favorite track = " + streamItem.getTrackItem() )

        file.close()

    def readFromFile(self, fileName, dataMyStreamList) :

        file = QFile(fileName)
        file.open(QIODevice.ReadOnly)
        datastream = QDataStream(file)

        printDebug("start read streamList")
        dataMyStreamList.streamList = datastream.readQVariant(  )
        dataMyStreamList.streamListFavorite = datastream.readQVariant(  )
        dataMyStreamList.streamListAdvert = datastream.readQVariant(  )

        try :
            for streamItem  in dataMyStreamList.streamList :
                #printDebug("loaded url = " + streamItem.url + " genre=" + streamItem.getGenre() + ", name =" +  streamItem.getName()  )
                for nowPlaying  in streamItem.nowPlaying :
                   printDebug("nowPlaying = " + nowPlaying.getTrackItem() +", date = " + nowPlaying.getTrackItemDt()  )
            #for streamItem  in dataMyStreamList.streamListFavorite :
            #   printDebug("* favorite track = " + streamItem.getTrackItem() )
        except :
            pass
        file.close()

class MyRec(QProcess) :

    def __init__(self,  parent=None):
        QProcess.__init__(self)
        self.parent = parent

    def startRecByStream(self, streamUrl) :
        printDebug("startRecByStream : " + streamUrl)
        self.streamUrl = streamUrl
        self.processCmd = QProcess(self)
        self.processCmd.readyReadStandardOutput.connect(self.stdoutReadyGetStatus)
        self.processCmd.readyReadStandardError.connect(self.stderrReadyGetStatus)
        argStr = '"streamripper" "'+streamUrl +'" "-d" "rip" "-u" "MyFreeAmp/1.2"'
        self.processCmd.start('bash', [ '-c' , argStr ])
        self.parent.parent.panelDisplay()
        
    def getStreamRec(self) :
        return self.streamUrl

    def stopRec(self) :
        self.processCmd.kill()

    #def stdoutReadyFinished(self):
    #    printDebug("startRecByStream finished : " + self.streamUrl)

    def stderrReadyGetStatus(self):
        data = self.processCmd.readAllStandardError().data()
        ch = str(data, encoding="utf-8").rstrip()
        printDebug(  "stderrReadyGetStatus : " + self.streamUrl + " :"  + ch)
        self.parent.errorRec( self.streamUrl , ch )

    def stdoutReadyGetStatus(self):
        data = self.processCmd.readAllStandardOutput().data()
        ch = str(data, encoding="utf-8").rstrip()
        printDebug(  "stdoutReadyGetStatus: " + self.streamUrl + " :" + ch)

class TrackItem() :

    def __init__(self, track, trackdt , streamUrl):
        self.track = track
        self.trackdt = trackdt
        self.streamUrl= streamUrl
        self.settingsTrackDateFormat= "%d-%m-%Y %H:%M"

    def setTrackItem(self , track):
        self.track = track
    def getTrackItem(self ) :
        return self.track

    def setTrackItemDt(self , trackdt):
        self.nowplayingdt = track
    def getTrackItemDt(self) :
        return self.trackdt.strftime( self.settingsTrackDateFormat)        

    def setStreamUrl(self , streamUrl):
        self.streamUrl = track
    def getStreamUrl(self ) :
        return self.streamUrl


class MyStream() :

    def __init__(self, url):
        self.url = url
        self.lasterror = "?"
        self.lasterrorRec = "?"
        self.state = "?"
        self.name = ""
        self.genre = ""
        self.description = ""
        self.logourl=""

        self.notice = ""
        self.stream = ""
        self.pub = ""
        self.kbps = ""
        self.freq = ""
        self.canaux = ""
        self.codec = ""
        self.type = ""
        self.server = ""

        self.info = ""
        self.file= ""

        self.isAdv= 0

        self.nowPlaying= []
        self.settingsMaxPlayingHistory = 16
        self.settingsTrackDateFormat= "%d-%m-%Y %H:%M"

    def addNowPlaying(self , nowplaying, nowplayingdt, ):
        nowPlayingElem = TrackItem( nowplaying, nowplayingdt, self.url)
        nowPlayingElem.settingsTrackDateFormat= self.settingsTrackDateFormat
        self.nowPlaying.append(nowPlayingElem)
        while  len(self.nowPlaying) > self.settingsMaxPlayingHistory :        
            self.nowPlaying.pop(0)

    def setUrl(self , url):
        self.url = url
    def getUrl( self ):
        return self.url

    def setState(self , state):
        self.state = state
    def getState( self ):
        return self.state

    def setLastError(self , lasterror):
        self.lasterror = lasterror
    def getLastError( self ):
        return self.lasterror

    def setLastErrorRec(self , lasterror):
        self.lasterrorRec = lasterror
    def getLastErrorRec( self ):
        return self.lasterrorRec

    def setName(self , name):
        self.name = name
    def getName(self ):
        return self.name

    def setLogo(self , logourl):
        self.logourl = logourl
    def getLogo( self ):
        return self.logourl

    def setGenre(self , genre):
        self.genre = genre
    def getGenre(self ):
        return self.genre

    def setDescription(self , description):
        self.description = description
    def getDescription( self ):
        return self.description

    def setInfo(self , info):
        self.info = info
    def getInfo( self ):
        return self.info

    def setFile(self , file):
        self.file = file
    def getFile( self ):
        return self.file

    def setNotice(self , notice):
        self.notice = notice
    def getNotice( self ):
        return self.notice

    def setStream(self , stream):
        self.notice = stream
    def getStream( self ):
        return self.stream

    def setPub(self , pub):
        self.pub= pub
    def getPub( self ):
        return self.pub

    def setKbps(self , kbps):
        self.kbps = kbps
    def getKbps( self ):
        return self.kbps

    def setFreq(self , freq):
        self.freq = freq
    def getFreq( self ):
        return self.freq

    def setCanaux(self , canaux):
        self.canaux = canaux
    def getCanaux( self ):
        return self.canaux

    def setCodec(self , codec):
        self.codec = codec
    def getCodec( self ):
        return self.codec

    def setType(self , type):
        self.type = type
    def getType( self ):
        return self.type

    def setServer(self , server):
        self.server = server
    def getServer( self ):
        return self.server

    def searchStationHome( self ):
        start = 'http'

        m = re.search(start+'(.+?)(\b|$)', self.name)
        if m:
            found = m.group(1)
            printDebug( start  + found )
            return start  + found

        m = re.search(start+'(.+?)(\b|$)', self.lasterror)
        if m:
            found = m.group(1)
            printDebug( start  + found )
            return start  + found

        m = re.search(start+'(.+?)(\b|$)', self.genre)
        if m:
            found = m.group(1)
            printDebug( start  + found )
            return start  + found

        m = re.search(start+'(.+?)(\b|$)', self.description)
        if m:
            found = m.group(1)
            printDebug( start  + found )
            return start  + found

        m = re.search(start+'(.+?)(\b|$)', self.server)
        if m:
            found = m.group(1)
            printDebug( start  + found )
            return start  + found

        m = re.search(start+'(.+?)(\b|$)', self.notice)
        if m:
            found = m.group(1)
            printDebug( start  + found )
            return start  + found

        return ""


class MyStreams() :

    def __init__(self, parent=None, name='MyStreams'):
        self.parent = parent
        self.init()

    def setSettings(self, settings) :
        self.settings = settings

    def init(self) :
        self.streamList= []
        self.streamListFavorite= []
        self.streamListAdvert= []
        self.streamListRec=[]
        self.lastAdded = "?"

    def initStreamList(self) :
        for streamItem  in self.streamList :
            streamItem.setState("?")

    def startRec(self, streamUrl) :
        myrecItem = MyRec(self)
        for recItem  in self.streamListRec :
            if streamUrl ==  recItem.getStreamRec() :
                return
        self.streamListRec.append(myrecItem)
        myrecItem.startRecByStream( streamUrl )

    def stopRec(self, streamUrl) :
        for recItem  in self.streamListRec :
            if streamUrl ==  recItem.getStreamRec() :
                reply = QMessageBox.question(self.parent, 'Confirmez',"Terminer l'enregistrement ?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
                if reply == QMessageBox.Yes:
                    recItem.stopRec()
                    self.streamListRec.remove(recItem)
                    self.parent.panelDisplay()
                    return 1
        return 0

    def errorRec( self, streamUrl , ch ) :
        for recItem  in self.streamListRec :
            if streamUrl ==  recItem.getStreamRec() :
                self.stopRec( streamUrl )
                self.setLastErrorRecByStream(streamUrl, ch)

    def isRecByStream(self, streamUrl) :
        for recItem  in self.streamListRec :
            if streamUrl ==  recItem.getStreamRec() :
                return 1
        return 0

    def addFavorite(self , nowplaying, nowplayingdt, streamUrl):
        #if nowplaying == "?" : 
        #    return
        for streamItem  in self.streamListFavorite :
            if nowplaying ==  streamItem.getTrackItem() and streamUrl == streamItem.getStreamUrl() :
                return
        favoriteElem = TrackItem(nowplaying, nowplayingdt, streamUrl)
        favoriteElem.settingsTrackDateFormat= self.settings.value("ui/configTrackDateFormat", "%d-%m-%Y %H:%M")
        self.streamListFavorite.append(favoriteElem)
        while  len(self.streamListFavorite) > int(self.settings.value("streams/configMaxFavoriteHistory", 999)) :
            self.streamListFavorite.pop(0)

    def delFavoriteByTrack(self , track):
        for streamItem  in self.streamListFavorite :
            if track ==  streamItem.getTrackItem() :
                self.streamListFavorite.remove(streamItem)

    def addAdvert(self , nowplaying, nowplayingdt, streamUrl ):
        printDebug("Add black list : " + nowplaying + " /" + streamUrl );
        printDebug( "nowplaying=[" + nowplaying + "]");
        if nowplaying == "?" : 
            return
        for streamItem  in self.streamListAdvert :            
            if nowplaying ==  streamItem.getTrackItem() and streamUrl == streamItem.getStreamUrl() :
                return
		
        pubElem = TrackItem( nowplaying, nowplayingdt, streamUrl)
        pubElem.settingsTrackDateFormat= self.settings.value("ui/configTrackDateFormat", "%d-%m-%Y %H:%M")
        self.streamListAdvert.append(pubElem )
        printDebug("Add black list pubElem : " + nowplaying + " /" + streamUrl );                
        while  len(self.streamListAdvert) > int(self.settings.value("streams/configMaxPubHistory", 100)) :
            self.streamListAdvert.pop(0)

    def delAdvertByTrack(self , track):
        for streamItem  in self.streamListAdvert :
            if track ==  streamItem.getTrackItem() :
                self.streamListAdvert.remove(streamItem)

    def isAdvertByTrackAndUrl(self , track, url):
        for streamItem  in self.streamListAdvert :
            if url ==  streamItem.getStreamUrl() and track ==  streamItem.getTrackItem()  :
                return 1
        return 0

    def  clear(self) :
        self.streamList= []
        self.streamListFavorite= []
        self.streamListAdvert= []
        self.streamListRec=[]

    def  addStream(self, url) :
        currentStream = MyStream( url )
        currentStream.settingsMaxPlayingHistory = int( self.settings.value("streams/configMaxNowPlayingHistory", 16) )
        currentStream.settingsTrackDateFormat = self.settings.value("ui/configTrackDateFormat", "%d-%m-%Y %H:%M")
        self.streamList.append(currentStream)
        self.lastAdded = url
 
    def  existsByStream(self, url) :
        try :
            for streamItem  in self.streamList :
                if  streamItem.url == url :
                    return 1
        except :
            pass

        return 0

    def  delAllByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                self.streamList.remove( streamItem )

    def  delByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                self.streamList.remove( streamItem )
                return

    def addNowPlayingByStream( self, url , nowplaying, nowplayingdt) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.addNowPlaying( nowplaying, nowplayingdt )
        if self.isAdvertByTrackAndUrl(nowplaying, url) == 1 :
            self.isAdv= 1
        else :
            self.isAdv= 0

    def getNowPlayingByStream( self, url ) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.nowPlaying

    def  setNameByStream(self, url, name) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setName( name )

    def  getNameByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getName()
        return ""

    def  setStateByStream(self, url, state) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setState( state )
            else :
                streamItem.setState( "?" )

    def  setStateAll(self,  state) :
        try :
            for streamItem  in self.streamList :
                streamItem.setState( state )
        except :
            pass

    def  getStateByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getState()
        return "?"

    def  setLogoByStream(self, url, logo) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setLogo( logo )

    def  getLogoByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getLogo()
        return ""

    def  setLastErrorByStream(self, url, lasterror) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setLastError( lasterror )

    def  getLastErrorByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getLastError()
        return ""

    def  setLastErrorRecByStream(self, url, lasterror) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setLastErrorRec( lasterror )

    def  getLastErrorRecByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getLastErrorRec()
        return ""

    def  setNoticeByStream(self, url, notice) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setNotice( notice )

    def  getNoticeByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getNotice()
        return ""

    def  setGenreByStream(self, url, genre) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setGenre( genre )

    def  getGenreByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getGenre()
        return ""

    def  setDescriptionByStream(self, url, description) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setDescription( description )

    def  getDescriptionByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getDescription()
        return ""

    def  setStreamByStream(self, url, stream) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setStream( stream )

    def  getStreamByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getStream()
        return ""

    def  setFileByStream(self, url, file) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setFile( file)

    def  getUrlByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getUrl()
        return ""

    def  setUrlByStream(self, url, stream) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setUrl( strem)

    def  getFileByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getFile()
        return ""

    def  setInfoByStream(self, url, info) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setFile( info)

    def  getInfoByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getInfo()
        return ""

    def  setPubByStream(self, url, pub) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setPub( pub )

    def  getPubByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getPub()
        return ""

    def  setKbpsByStream(self, url, kbps) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setKbps( kbps )

    def  getKbpsByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getKbps()
        return ""

    def  setFreqByStream(self, url, freq) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setFreq( freq )

    def  getFreqByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getFreq()
        return ""

    def  setCanauxByStream(self, url, canaux) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setCanaux( canaux )

    def  getCanauxByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getCanaux()
        return ""

    def  setCodecByStream(self, url, codec) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setCodec( codec )

    def  getCodecByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getCodec()
        return ""

    def  setTypeByStream(self, url, type) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setType( type )

    def  getTypeByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getType()
        return ""

    def  setServerByStream(self, url, server) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                streamItem.setServer( server )

    def  getServerByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.getServer()
        return ""

    def  getCount(self ) :
        return len(self.streamList)

    def searchStationHomeByStream(self, url) :
        for streamItem  in self.streamList :
            if  streamItem.url == url :
                return streamItem.searchStationHome()
        return ""

    def  dumpList(self) :
        for streamItem  in self.streamList :
            printDebug("url = " + streamItem.getUrl() + " genre=" + streamItem.getGenre() + ", name =" +  streamItem.getName()  )
            for nowPlaying  in streamItem.nowPlaying :
                printDebug("nowPlaying = " + nowPlaying.getTrackItem() + ", date = " + nowPlaying.getTrackItemDt()  )
        for streamItem  in self.streamListFavorite :
           printDebug("favorite track = " + streamItem.getTrackItem() )


class dialogProperty(QDialog):

    def __init__(self, parent,  settings, url ):
        self.url = url
        self.parent = parent
        self.settings = settings
        super().__init__(parent)
        self.initUI()

    def initUI(self):

        okButton = QPushButton("OK")
        cancelButton = QPushButton("Cancel")

        buttonBoxLayout = QHBoxLayout()
        buttonBoxLayout.addWidget(okButton)
        buttonBoxLayout.addWidget(cancelButton)

        okButton.clicked.connect( self.onOkButton)
        cancelButton.clicked.connect( self.onCancelButton)

        vbox = QGridLayout()

        labelFlux = QLabel('Flux')
        vbox.addWidget(labelFlux,0,0 )
        self.editFlux = QLineEdit()
        vbox.addWidget(self.editFlux,0,1)
        self.editFlux.setText(  self.url )

        labelName = QLabel('Nom')
        vbox.addWidget(labelName ,1,0 )
        self.editName = QLineEdit()
        vbox.addWidget(self.editName,1,1)
        self.editName.setText(  self.parent.parent.streamList.getNameByStream( self.url ) )

        labelErr = QLabel('Erreur lecture')
        vbox.addWidget(labelErr,2,0 )
        self.editErr = QLineEdit()
        vbox.addWidget(self.editErr,2,1)
        self.editErr.setText(  self.parent.parent.streamList.getLastErrorByStream( self.url ) )

        labelErrRec = QLabel('Erreur enr.')
        vbox.addWidget(labelErrRec,3,0 )
        self.editErrRec = QLineEdit()
        vbox.addWidget(self.editErrRec,3,1)
        self.editErrRec.setText(  self.parent.parent.streamList.getLastErrorRecByStream( self.url ) )

        labelState = QLabel('Etat')
        vbox.addWidget(labelState ,4,0 )
        self.editState = QLineEdit()
        vbox.addWidget(self.editState,4,1)
        self.editState.setText(  self.parent.parent.streamList.getStateByStream( self.url ) )

        labelGenre = QLabel('Genre')
        vbox.addWidget(labelGenre ,5,0 )
        self.editGenre = QLineEdit()
        vbox.addWidget(self.editGenre,5,1)
        self.editGenre.setText(  self.parent.parent.streamList.getGenreByStream( self.url ) )

        labelDescription = QLabel('Description')
        vbox.addWidget(labelDescription ,6,0 )
        self.editDescription = QLineEdit()
        vbox.addWidget(self.editDescription,6,1)
        self.editDescription.setText(  self.parent.parent.streamList.getDescriptionByStream( self.url ) )

        labelNotice = QLabel('Notice')
        vbox.addWidget(labelNotice ,7,0 )
        self.editNotice = QLineEdit()
        vbox.addWidget(self.editNotice,7,1)
        self.editNotice.setText(  self.parent.parent.streamList.getNoticeByStream( self.url ) )

        labelStream = QLabel('Stream')
        vbox.addWidget(labelStream ,8,0 )
        self.editStream = QLineEdit()
        vbox.addWidget(self.editStream,8,1)
        self.editStream.setText(  self.parent.parent.streamList.getStreamByStream( self.url ) )

        labelPub = QLabel('Pub')
        vbox.addWidget(labelPub ,9,0 )
        self.editPub = QLineEdit()
        vbox.addWidget(self.editPub,9,1)
        self.editPub.setText(  self.parent.parent.streamList.getPubByStream( self.url ) )

        labelKbps = QLabel('Kbps')
        vbox.addWidget(labelKbps ,10,0 )
        self.editKbps = QLineEdit()
        vbox.addWidget(self.editKbps,10,1)
        self.editKbps.setText(  self.parent.parent.streamList.getKbpsByStream( self.url ) )

        labelFreq = QLabel('Freq')
        vbox.addWidget(labelFreq ,11,0 )
        self.editFreq = QLineEdit()
        vbox.addWidget(self.editFreq,11,1)
        self.editFreq.setText(  self.parent.parent.streamList.getFreqByStream( self.url ) )

        labelCanaux = QLabel('Canaux')
        vbox.addWidget(labelCanaux ,12,0 )
        self.editCanaux = QLineEdit()
        vbox.addWidget(self.editCanaux,12,1)
        self.editCanaux.setText(  self.parent.parent.streamList.getCanauxByStream( self.url ) )

        labelCodec = QLabel('Codec')
        vbox.addWidget(labelCodec ,13,0 )
        self.editCodec = QLineEdit()
        vbox.addWidget(self.editCodec,13,1)
        self.editCodec.setText(  self.parent.parent.streamList.getCodecByStream( self.url ) )

        labelType = QLabel('Type')
        vbox.addWidget(labelType ,14,0 )
        self.editType = QLineEdit()
        vbox.addWidget(self.editType,14,1)
        self.editType.setText(  self.parent.parent.streamList.getTypeByStream( self.url ) )

        labelServer = QLabel('Server')
        vbox.addWidget(labelServer ,15,0 )
        self.editServer = QLineEdit()
        vbox.addWidget(self.editServer,15,1)
        self.editServer.setText(  self.parent.parent.streamList.getServerByStream( self.url ) )

        labelServer = QLabel('Logo')
        vbox.addWidget(labelServer ,16,0 )
        self.logoButton = QPushButton("...")
        self.logoButton.clicked.connect( self.onLogoButton)
        vbox.addWidget(self.logoButton,16,1)

        nowPlayingTb  = NowPlayingHistoryTab(self, self.settings)
        nowPlayingPtr = self.parent.parent.streamList.getNowPlayingByStream( self.url )
        for nowPlayingItem  in  nowPlayingPtr[::-1] :
            rowPosition = nowPlayingTb.rowCount()
            nowPlayingTb.insertRow(rowPosition)
            nowPlayingTb.setItem(rowPosition , 0, QTableWidgetItem(nowPlayingItem.getTrackItem()))
            nowPlayingTb.setItem(rowPosition , 1, QTableWidgetItem(nowPlayingItem.getTrackItemDt()))

        nowPlayingTb.resizeColumnsToContents();
        header = nowPlayingTb.horizontalHeader()
        header.setStretchLastSection(True)
        vbox.addWidget(nowPlayingTb ,17,0,1,2)

        vbox.addLayout(buttonBoxLayout,18,0,1,2)

        self.setLayout(vbox)
        self.setWindowTitle("Propriétés du flux")
        self.setModal(True)
        self.setGeometry(150, 150, 650, 800)

    def onLogoButton(self) :
        fname = QFileDialog.getOpenFileName(self, 'Selectionner le logo de la station ', '.')
        if fname[0]:
            self.parent.parent.streamList.setLogoByStream( self.url, fname[0] )
            self.parent.parent.centralTableWidget.displayData()

    def onCancelButton(self, event) :
        self.close()

    def onOkButton (self, event) :
        self.close()

class NowPlayingHistoryTab( QtWidgets.QTableWidget) :

    def __init__(self, parent=None, settings=None, name='NowPlayingHistoryTab'):
        super(NowPlayingHistoryTab, self).__init__(parent)
        self.name = name
        self.parent= parent
        self.settings = settings
        #super(QSettings , self.settings).__init__()

        self.setAlternatingRowColors(True)
        self.setRowCount(0)
        self.setColumnCount(2)
        self.setHorizontalHeaderLabels(["Piste", "Date"])

    def contextMenuEvent(self, event):

        if self.currentIndex().row() >=0 :

            menu = QtWidgets.QMenu()

            ActionAddFavorites = menu.addAction("Ajouter aux favoris")
            ActionAddFavorites.triggered.connect(self.ctxAddFavorites)
            ActionAddAsPub = menu.addAction("Ajouter à la liste noire")
            ActionAddAsPub.triggered.connect(self.ctxAddAsAdvert)
            ActionSearchWeb = menu.addAction("Rechercher ...")
            ActionSearchWeb .triggered.connect(self.ctxSarchWeb)
            self.cursor_zero = event.pos()
            menu.exec_(event.globalPos())

    def ctxSarchWeb(self):
        try:
            printDebug("search adress = " + self.settings.value("functions/configSearchWeb", "http://pleer.net/search?q=")  )
            itemText = self.item(self.currentIndex().row(), 0)        
            webbrowser.open(self.settings.value("functions/configSearchWeb", "http://pleer.net/search?q=")  + itemText.text())
        except:
            pass


    def ctxAddAsAdvert(self):
        itemText = self.item(self.currentIndex().row(), 0)        
        self.parent.parent.parent.streamList.addAdvert( itemText.text(), datetime.datetime.now() , self.parent.url)

    def ctxAddFavorites(self):
        itemText = self.item(self.currentIndex().row(), 0)        
        self.parent.parent.parent.streamList.addFavorite( itemText.text(), datetime.datetime.now(),  self.parent.url )


class AdvertsTab( QtWidgets.QTableWidget) :

    def __init__(self,  parent=None, name='AdvertsTab'):
        super(AdvertsTab, self).__init__(parent)
        self.name = name
        self.parent= parent

        self.setAlternatingRowColors(True)
        self.setRowCount(0)
        self.setColumnCount(3)
        self.setHorizontalHeaderLabels(["Piste", "Date","Station"])

    def contextMenuEvent(self, event):

        if self.currentIndex().row() >=0 :

            menu = QtWidgets.QMenu()

            ActionAddAsPub = menu.addAction("Déplacer vers favoris")
            ActionAddAsPub.triggered.connect(self.ctxAddAsFavorite)

            ActionRemove = menu.addAction("Supprimer")
            ActionRemove.triggered.connect(self.ctxRemove)
            self.cursor_zero = event.pos()
            menu.exec_(event.globalPos())

    def ctxAddAsFavorite(self):
        itemText = self.item(self.currentIndex().row(), 0)        
        trackName  = itemText.text()
        itemText = self.item(self.currentIndex().row(), 2)        
        station  = itemText.text()
        self.parent.parent.streamList.addFavorite( trackName , datetime.datetime.now(), station )	
        self.parent.parent.streamList.delAdvertByTrack( trackName )
        self.parent.AdvertTbClear()
        self.parent.AdvertTbDisplayData()

    def ctxRemove(self):
        itemText = self.item(self.currentIndex().row(), 0)        
        trackName  = itemText.text()
        reply = QMessageBox.question(self, 'Confirmez', "Confirmez-vous la supression de : \n\n" + trackName   , QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.parent.parent.streamList.delAdvertByTrack( trackName )
            self.parent.AdvertTbClear() 
            self.parent.AdvertTbDisplayData() 

class dialogAdverts(QDialog):

    def __init__(self, parent ):
        self.parent = parent
        super().__init__(parent)
        self.initUI()

    def initUI(self):

        okButton = QPushButton("OK")
        cancelButton = QPushButton("Cancel")

        buttonBoxLayout = QHBoxLayout()
        buttonBoxLayout.addWidget(okButton)
        buttonBoxLayout.addWidget(cancelButton)

        okButton.clicked.connect( self.onOkButton)
        cancelButton.clicked.connect( self.onCancelButton)

        vbox = QGridLayout()

        self.AdvertTb = AdvertsTab(self)
        self.AdvertTbDisplayData()

        vbox.addWidget(self.AdvertTb,0,0,1,2)

        vbox.addLayout(buttonBoxLayout,1,0,1,2)

        self.setLayout(vbox)
        self.setWindowTitle("Liste noire")
        self.setModal(True)
        self.setGeometry(150, 150, 650, 800)

    def AdvertTbClear(self) :
        self.AdvertTb.clearContents()
        self.AdvertTb.setRowCount( 0 )

    def AdvertTbDisplayData(self) :
       
        advertPtr = self.parent.streamList.streamListAdvert
        for advertItem  in  advertPtr[::-1] :
            rowPosition = self.AdvertTb.rowCount()
            self.AdvertTb.insertRow(rowPosition)
            self.AdvertTb.setItem(rowPosition , 0, QTableWidgetItem(advertItem.getTrackItem()))
            self.AdvertTb.setItem(rowPosition , 1, QTableWidgetItem(advertItem.getTrackItemDt()))
            self.AdvertTb.setItem(rowPosition , 2, QTableWidgetItem(advertItem.getStreamUrl()))

        self.AdvertTb.resizeColumnsToContents();
        header = self.AdvertTb.horizontalHeader()
        header.setStretchLastSection(True)

    def onLogoButton(self) :
        fname = QFileDialog.getOpenFileName(self, 'Selectionner le logo de la station ', '.')
        if fname[0]:
            printDebug(fname[0])
            self.parent.parent.streamList.setLogoByStream( self.url, fname[0] )
            self.parent.parent.centralTableWidget.displayData()

    def onCancelButton(self, event) :
        self.close()

    def onOkButton (self, event) :
        self.close()

class FavoritesTab( QtWidgets.QTableWidget) :

    def __init__(self,  parent=None, name='FavoritesTab'):
        super(FavoritesTab, self).__init__(parent)
        self.name = name
        self.parent= parent

        self.setAlternatingRowColors(True)
        self.setRowCount(0)
        self.setColumnCount(3)
        self.setHorizontalHeaderLabels(["Piste", "Date","Station"])

    def contextMenuEvent(self, event):

        if self.currentIndex().row() >=0 :

            menu = QtWidgets.QMenu()

            ActionAddAsPub = menu.addAction("Déplacer vers la liste noire")
            ActionAddAsPub.triggered.connect(self.ctxAddAsPub)

            ActionSearchWeb = menu.addAction("Rechercher ...")
            ActionSearchWeb .triggered.connect(self.ctxSarchWeb)

            ActionRemove = menu.addAction("Supprimer")
            ActionRemove.triggered.connect(self.ctxRemove)
            self.cursor_zero = event.pos()
            menu.exec_(event.globalPos())

    def ctxAddAsPub(self):
        try:
            itemText = self.item(self.currentIndex().row(), 0)        
            trackName  = itemText.text()
            itemText = self.item(self.currentIndex().row(), 2)        
            station  = itemText.text()
            self.parent.parent.streamList.addAdvert( trackName , datetime.datetime.now(), station )	
            self.parent.parent.streamList.delFavoriteByTrack( trackName )
            self.parent.favoriteTbClear()
            self.parent.favoriteTbDisplayData()
        except:
            pass

    def ctxSarchWeb(self):
        printDebug("ctxSarchWeb @row " + str(self.currentIndex().row()))
        try:
            itemText = self.item(self.currentIndex().row(), 0)        
            webbrowser.open(self.settings.value("functions/configSearchWeb", "http://pleer.net/search?q=")  + itemText.text())
        except:
            pass

    def ctxRemove(self):
        try:
            itemText = self.item(self.currentIndex().row(), 0)        
            trackName  = itemText.text()
            reply = QMessageBox.question(self, 'Confirmez', "Confirmez-vous la supression de : \n\n" + trackName   , QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.parent.parent.streamList.delFavoriteByTrack( trackName )
                self.parent.favoriteTbClear() 
                self.parent.favoriteTbDisplayData() 
        except:
            pass

class dialogFavorite(QDialog):

    def __init__(self, parent ):
        self.parent = parent
        super().__init__(parent)
        self.initUI()

    def initUI(self):

        okButton = QPushButton("OK")
        cancelButton = QPushButton("Cancel")

        buttonBoxLayout = QHBoxLayout()
        buttonBoxLayout.addWidget(okButton)
        buttonBoxLayout.addWidget(cancelButton)

        okButton.clicked.connect( self.onOkButton)
        cancelButton.clicked.connect( self.onCancelButton)

        vbox = QGridLayout()

        self.favoriteTb = FavoritesTab(self)
        self.favoriteTbDisplayData()

        vbox.addWidget(self.favoriteTb,0,0,1,2)

        vbox.addLayout(buttonBoxLayout,1,0,1,2)

        self.setLayout(vbox)
        self.setWindowTitle("Favoris")
        self.setModal(True)
        self.setGeometry(150, 150, 650, 800)

    def favoriteTbClear(self) :
        self.favoriteTb.clearContents()
        self.favoriteTb.setRowCount( 0 )

    def favoriteTbDisplayData(self) :
       
        favoritePtr = self.parent.streamList.streamListFavorite
        for favoriteItem  in  favoritePtr[::-1] :
            rowPosition = self.favoriteTb.rowCount()
            self.favoriteTb.insertRow(rowPosition)
            self.favoriteTb.setItem(rowPosition , 0, QTableWidgetItem(favoriteItem.getTrackItem()))
            self.favoriteTb.setItem(rowPosition , 1, QTableWidgetItem(favoriteItem.getTrackItemDt()))
            self.favoriteTb.setItem(rowPosition , 2, QTableWidgetItem(favoriteItem.getStreamUrl()))

        self.favoriteTb.resizeColumnsToContents();
        header = self.favoriteTb.horizontalHeader()
        header.setStretchLastSection(True)

    def onLogoButton(self) :
        fname = QFileDialog.getOpenFileName(self, 'Selectionner le logo de la station', '.')
        if fname[0]:
            self.parent.parent.streamList.setLogoByStream( self.url, fname[0] )
            self.parent.parent.centralTableWidget.displayData()

    def onCancelButton(self, event) :
        self.close()

    def onOkButton (self, event) :
        self.close()

class dialogAddNewStream(QDialog):

    def __init__(self, parent, streamUrl="http://" ):
        self.parent = parent
        clipboard = QApplication.clipboard() 
        mimeData = clipboard.mimeData()
        self.streamUrl = clipboard.text().strip()
        super().__init__(parent)
        self.initUI()

    def initUI(self):

        okButton = QPushButton("OK")
        cancelButton = QPushButton("Cancel")

        buttonBoxLayout = QHBoxLayout()
        buttonBoxLayout.addWidget(okButton)
        buttonBoxLayout.addWidget(cancelButton)

        okButton.clicked.connect( self.onOkButton)
        cancelButton.clicked.connect( self.onCancelButton)

        vbox = QGridLayout()

        labelFileName = QLabel('Nouveau Flux')
        vbox.addWidget(labelFileName,0,0 )
        self.editStreamAdress = QLineEdit()
        vbox.addWidget(self.editStreamAdress,0,1)
        self.editStreamAdress.setText(  self.streamUrl )

        vbox.addLayout(buttonBoxLayout,1,0,1,2)

        self.setLayout(vbox)
        self.setWindowTitle("Création d'un nouveau flux")
        self.setModal(True)
        self.setGeometry(300, 300, 690, 150)

    def onCancelButton(self, event) :
        self.close()        

    def onOkButton (self, event) :

        doAdd=1
        if  self.parent.parent.streamList.existsByStream(self.editStreamAdress.text().strip()) == 1 :

            reply = QMessageBox.question(self, 'Confirmez', "Confirmez-vous l'ajout de ce flux en double: \n\n" + self.editStreamAdress.text().strip()   , QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:
                doAdd=1
            else :
                doAdd=0

        if doAdd == 1 :
            self.parent.parent.streamList.addStream( self.editStreamAdress.text().strip() )
            self.parent.parent.centralTableWidget.displayData()

        self.close()      

class MyPlayer(QWidget) :

    def __init__(self, parent=None, settings=None, name='MyPlayer'):
        super(MyPlayer, self).__init__(parent)
        self.resetInfo()
        self.name = name
        self.parent = parent
        self.settings = settings
        self.audiovolumeAdv = "?"
        self.audioVolumeBeforeAd = ""

    def resetInfo(self):
        self.url = "?"
        self.state = "?"
        self.lasterror = "?"
        self.newinput="?"
        self.bytesread="?"
        self.audiovolume = "?"
        self.kbs = "?"
        self.freq = "?"
        self.canaux = "?"
        self.codec = "?"
        self.type = "?"
        self.nowplaying = "?"
        self.nowplayingInfos = "?"
        self.nowplayingdt = "?"
        self.server="?"
        self.icy_url="?"
        self.icy_description="?"
        self.icy_pub="?"
        self.icy_name="?"
        self.icy_genre="?"
        self.icy_notice="?"

    def box(self, title, message):
        buttonReply = QMessageBox.question(self, title, message, QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if buttonReply == QMessageBox.Yes:
            printDebug('Yes clicked.')
            return 1
        else:
            printDebug('No clicked.')
            return 0

    def reset(self):
        if self.box("Reset player", "Confirmez le reset de la configuration de vlc") == 1 :
            commandStr = 'vlc --reset-config' ;
            printDebug(commandStr)
            command = commandStr.split()
            subprocess.Popen( command )

    def isPlayingAdv(self):
        return self.parent.streamList.isAdvertByTrackAndUrl( self.nowplaying , self.url)

    def DEL_setAudioVolumeBeforeAd( self  ) :
        printDebug( " Volume was before Ad : " + str(self.audiovolume ))
        self.audioVolumeBeforeAd = self.audiovolume 
        printDebug( " Volume was before Ad : " + str(self.audiovolume ))

    def playUrl(self, url):
        printDebug("playUrl " + url )
        if url.startswith('http:') :
            commandStr = '-vv --intf rc --rc-host localhost:8082 ' + url
            printDebug(commandStr)
            commandParam = commandStr.split()
            self.resetInfo()
            self.processPlay = QProcess(self)
            self.processPlay.readyReadStandardOutput.connect(self.stdoutReadyPlay)
            self.processPlay.readyReadStandardError.connect(self.stderrReadyPlay)
            self.processPlay.start('cvlc', commandParam)
            self.url = url
            self.lasterror = "?"
            self.parent.streamList.setLastErrorByStream( url, "?")

    def stdoutReadyPlay(self):
        data = self.processPlay.readAllStandardOutput().data()
        ch = str(data, encoding="utf-8").rstrip()
        self.lastStdout = ch
        self.lastStdout = self.lastStdout + "\n@" #  add @ : regex bug fix ; regex dont match last char, if last char is new line
        if int(self.settings.value("debug/configDebugToFile", 0)) == 1 :
            with open('log.txt', 'a+') as logFile:
                logFile.write("stdOut = " + self.lastStdout)

    def stderrReadyPlay(self):

        data = self.processPlay.readAllStandardError().data()
        ch = str(data, encoding="utf-8").rstrip()
        self.lastStdout = ch
        self.lastStdout = self.lastStdout + "\n@"

        if int(self.settings.value("debug/configDebugToFile", 0)) == 1 :
            with open('log.txt', 'a+') as logFile:
                logFile.write("stdErr = " + self.lastStdout)

        errDet1=""
        posErr1 = self.lastStdout.find('connection failed')
        if posErr1 > 0  :
            errDet1 = "connection failed"
        errDet2=""
        posErr2 = self.lastStdout.find('error: cannot connect to')
        if posErr2 > 0  :
            errDet2 = "cannot connect"
        errDet3=""
        posErr3 = self.lastStdout.find('error: authentication failed without realm')
        if posErr3 > 0  :
            errDet3 = "authentication failed without realm"
        errDet4=""
        posErr4 = self.lastStdout.find('authentication failed')
        if posErr4 > 0  :
            errDet4 = "authentication failed"

        if posErr1 >=0  or posErr2 >=0 or posErr3 >=0 or posErr4 >=0 :
            self.lasterror = "Erreur(s) de lecture du flux : " + errDet1 + "; " + errDet2 + "; "+ errDet3 + "; "+ errDet4
            printDebug('Err=' + self.lasterror )
            self.parent.streamList.setLastErrorByStream( self.url, self.lasterror)

        re = QRegExp(r'Server: (.*)')
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.server=info
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.streamList.setServerByStream( self.url, self.server)

        re = QRegExp(r'Meta-Info: icy-url: (.*)')
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.icy_url=info
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.streamList.setStreamByStream( self.url, self.icy_url)

        re = QRegExp(r'Meta-Info: icy-description: (.*)')
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.icy_description=info
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.streamList.setDescriptionByStream( self.url, self.icy_description)

        re = QRegExp(r'Meta-Info: icy-pub:(.*)')
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.icy_pub=info
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.streamList.setPubByStream( self.url, self.icy_pub)

        re = QRegExp(r'Icy-Name: (.*)' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.icy_name=info
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.streamList.setNameByStream( self.url, self.icy_name)

        re = QRegExp(r'Icy-Genre: (.*)' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.icy_genre=info
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.streamList .setGenreByStream( self.url, self.icy_genre)

        re = QRegExp(r'Icy-Notice: (.*)' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            printDebug(" self.lastStdout=" + self.lastStdout)
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.icy_notice=info
            self.icy_notice = self.icy_notice.replace("<BR>","")
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.streamList.setNoticeByStream( self.url, self.icy_notice)

        re = QRegExp(r'New Icy-Title=(.*)' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.nowplaying=info
            pos = re.indexIn(self.lastStdout, pos +1)
            posStartInfo =  info.find(r'{+info: ')
            if posStartInfo > 0 :
                posEndinfo =  info.find("}")
                self.nowplayingInfos = info[posStartInfo+8:posEndinfo]
                self.nowplaying=self.nowplaying[:posStartInfo ].strip()
            self.nowplayingdt=datetime.datetime.now()
            self.parent.streamList.addNowPlayingByStream( self.url, self.nowplaying, self.nowplayingdt)
            self.parent.onChangeNowPlaying()

        isPlaying= self.lastStdout.find(r'pulse audio output debug: started')
        if isPlaying > -1 :
            self.grabInfos()

    def grabInfos(self):
        self.currentCommand="status"
        self.getVlcInfo("status")

    def getVlcInfo(self, commandStr ):
        self.processCmd = QProcess(self)
        self.processCmd.readyReadStandardOutput.connect(self.stdoutReadyGetStatus)
        self.processCmd.readyReadStandardError.connect(self.stderrReadyGetStatus)
        self.processCmd.finished.connect(self.stdoutReadyFinished)
        argStr = '"echo" "'+commandStr+'" | "nc" "localhost" "8082"'
        self.processCmd.start('bash', [ '-c' , argStr ])

    def stdoutReadyFinished(self):

        if self.currentCommand=="status" :
            self.currentCommand="info"
            self.getVlcInfo("info")
            return

        if self.currentCommand=="info" :
            self.currentCommand="stats"
            self.getVlcInfo("stats")
            return

        if self.currentCommand=="stats" :
            self.parent.panelDisplay()
            #if self.isPlayingAdv() != 1 :
            #    self.audioVolumeBeforeAd = self.audiovolume
            #    printDebug( "SET self.audioVolumeBeforeAd=" + str(self.audioVolumeBeforeAd))            
            return



    def stdoutReadyGetStatus(self):
        data = self.processCmd.readAllStandardOutput().data()
        ch = str(data, encoding="utf-8").rstrip()
        self.lastStdout = ch
        self.lastStdout = self.lastStdout + "\n@"
        if int(self.settings.value("debug/configDebugToFile", 0)) == 1 :
            with open('log.txt', 'a+') as logFile:
                logFile.write("stdOut vlc command = " + self.lastStdout)

        # ............. status ..............................
        re = QRegExp(r'state(.*)\r' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find(")")
            info = self.info[:posEnd ]
            self.state=info.strip()
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.panelDisplay()
            self.parent.streamList .setStateByStream( self.url, self.state)

        re = QRegExp(r'audio volume: (.*)\r' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find(")")
            info = self.info[:posEnd ]
            self.audiovolume=info.strip()
            #    self.audiovolumeAdv = self.audiovolume
            #if self.audiovolumeAdv  == "?" : 
            #    self.audiovolumeAdv = self.audiovolume
            print ("new Volume=["  + self.audiovolume+ "]")
            if self.isPlayingAdv() != "1" or self.audioVolumeBeforeAd == "" :
                self.audioVolumeBeforeAd = self.audiovolume            
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.panelDisplay()

        re = QRegExp(r'new input: (.*)\r' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find(")")
            info = self.info[:posEnd ].strip()
            self.newinput=info.strip()
            pos = re.indexIn(self.lastStdout, pos +1)
        # ............. status ..............................

        # ............. info ..............................
        re = QRegExp(r'Codec : (.*)\r' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.codec=info.strip()
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.streamList .setCodecByStream( self.url, self.codec)

        re = QRegExp(r'Débit : (.*)\r' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.kbs=info.strip()
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.streamList.setKbpsByStream( self.url, self.kbs)

        re = QRegExp(r'échantillonnage: (.*)\r' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.freq=info.strip()
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.streamList.setFreqByStream( self.url, self.freq)

        re = QRegExp(r'Type : (.*)\r' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.type=info.strip()
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.streamList.setTypeByStream( self.url, self.type)

        re = QRegExp(r'Canaux : (.*)\r' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ].strip()
            self.canaux=info.strip()
            pos = re.indexIn(self.lastStdout, pos +1)
            self.parent.streamList.setCanauxByStream( self.url, self.canaux)
        # ............. info ..............................

        # ............. stats ..............................
        re = QRegExp(r'input bytes read :(.*)\r' )
        pos = re.indexIn(self.lastStdout)
        count = 0;
        while pos != -1 :
            count = count + 1
            captureTxt = re.capturedTexts()
            self.info = captureTxt[1]
            posEnd =  self.info.find("\n")
            info = self.info[:posEnd ]
            self.bytesread=info.strip()
            pos = re.indexIn(self.lastStdout, pos +1)
        # ............. stats ..............................


    def stderrReadyGetStatus(self):
        data = processCmd.readAllStandardError().data()
        ch = str(data, encoding="utf-8").rstrip()
        self.lastStdout = ch
        self.lastStdout = self.lastStdout + "\n@"
        self.lasterror = self.lastStdout
        printDebug('Error parsing vlc command : ' + self.lastStdout)
        if int(self.settings.value("debug/configDebugToFile", 0)) == 1 :
            with open('log.txt', 'a+') as logFile:
                logFile.write("stdErr vlc command = " + self.lastStdout)

    def shutdownForce(self):
        p1 = subprocess.Popen(["echo", "shutdown"], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["nc", "localhost", "8082"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        p2.communicate()

    def shutdown(self, title="Shutdown player", message="Confirmez la fermeture du lecteur"):
        if self.box(title, message) == 1 :
            self.shutdownForce()
            self.resetInfo()
            return 1
        else:
            return 0

    def togglePause(self):
        p1 = subprocess.Popen(["echo", "pause"], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["nc", "localhost", "8082"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        p2.communicate()

    def voldown(self):
        printDebug("volCmd Dwon="  )
        p1 = subprocess.Popen(["echo", "voldown"], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["nc", "localhost", "8082"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        p2.communicate()

    def volup(self):
        printDebug("volCmd UP " )
        p1 = subprocess.Popen(["echo", "volup"], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["nc", "localhost", "8082"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        p2.communicate()

    def vol(self, level):                
        volCmd="volume " + str(level)
        printDebug("volCmd=" + volCmd )
        p1 = subprocess.Popen(["echo", volCmd ], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["nc", "localhost", "8082"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        p2.communicate()

    def DEL_volAdv(self):       
        #if (self.isPlayingAdv() == 1 or  self.audiovolumeAdv  == "?")  : 
        #if self.isPlayingAdv()  == 1  : 
        #    self.audiovolumeAdv = self.audiovolume
        #self.audiovolumeAdv = self.audiovolume
        printDebug("Change player volume ADV : isPlayingAdv=" + str(self.isPlayingAdv()) )
        volCmd="volume " + str(int(self.settings.value("streams/configVolumeAd", 10)))
        printDebug("set volCmd to : " + volCmd )        
        p1 = subprocess.Popen(["echo", volCmd ], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["nc", "localhost", "8082"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        p2.communicate()

    def DEL_volBack(self):
        #if (self.isPlayingAdv() == 1 or  self.audiovolumeAdv  == "?")  : 
        #    return		
		#def setAudioVolumeBeforeAd( self, volumeVal  ) :
		#self.audioVolumeBeforeAd = volumeVal        
        printDebug("Volume back : isPlayingAdv ? =" + str(self.isPlayingAdv()) )
        #volCmd="volume " + str(self.audiovolumeAdv)        
        volCmd="volume " + self.audioVolumeBeforeAd
        printDebug("volCmd back : " + self.audioVolumeBeforeAd )
        p1 = subprocess.Popen(["echo", volCmd ], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["nc", "localhost", "8082"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        p2.communicate()


class ImgStreamLogo( QLabel ):

    def __init__(self, settings=None, parent=None ):
        super(ImgStreamLogo, self).__init__(parent)
        self.settings = settings
        self.resize(int(self.settings.value("ui/configLogoW", 64)), int(self.settings.value("ui/configLogoH", 64)))

    def getLogo(self, fullFileName ):
        if os.path.isfile(fullFileName) : 
            self.pic = QPixmap( fullFileName )
            self.setPixmap(self.pic.scaled(self.width(), self.height(), Qt.KeepAspectRatio))
            return self
        else :
            return self

class MyTableWidget(QtWidgets.QTableWidget):

    def __init__(self, parent=None , settings=None , name='Table1'):
        super(MyTableWidget, self).__init__(parent)
        self.name = name
        self.parent= parent
        self.settings = settings

        self.cellClicked.connect(self.cell_was_clicked)

        self.setGeometry(0,0,300, 300)
        self.setAlternatingRowColors(True)
        self.setFont(QFont( self.settings.value("ui/configFontFamilly", "Arial"), int(self.settings.value("ui/configFontSize", 12))))

        self.itemSelectionChanged.connect(self.onCellSeleted)

        self.dirPath = ""

        self.row = -1
        self.col = -1
        self.text = ""
        self.streamUrl = ""
        self.streamUrlRow = -1

        self.countUrl=0

        self.audiovolume = -1

        header = self.horizontalHeader()
        header.sortIndicatorChanged.connect(self.onSortEvent)
        self.horizontalHeader().sectionClicked.connect(self.onClickHeader)

        self.setSelectionMode(QAbstractItemView.ExtendedSelection)

    def onClickHeader(self) :
        self.setSortingEnabled(False)
        self.clearSelection()

    def onSortEvent(self,  index, order) :
        self.setSortingEnabled(True)

    def clear( self ) :
        self.clearContents()
        self.setRowCount( 0 )
 
    def onCellSeleted(self):
        for currentQTableWidgetItem in self.selectedItems():
            self.row = currentQTableWidgetItem.row()
            self.col = currentQTableWidgetItem.column()
            try:
                item = self.item(self.row, self.col)
                itemTxt = item.text()
            except:
                itemTxt = ""
                pass

            printDebug( "self.row/col, txt=" + str(self.row) + "/" + str(self.col) + "," + itemTxt )

    def contextMenuEvent(self, event):

        menu = QtWidgets.QMenu()

        if self.currentIndex().row() >=0 :

            ActionPlay = menu.addAction("Ecouter")
            ActionPlay.triggered.connect(self.ctxStreamPlay)

            ActionProperty = menu.addAction("Propriétés")
            ActionProperty.triggered.connect(self.ctxProperty)

            ActionGotoHomeStation = menu.addAction("Aller sur la page web de la station")
            ActionGotoHomeStation.triggered.connect(self.ctxSearchStationHome )

            ActionRemoveStream = menu.addAction("Supprimer le flux")
            ActionRemoveStream.triggered.connect(self.ctxStreamRemove)

            ActionAddStream = menu.addAction("Nouveau flux")
            ActionAddStream.triggered.connect(self.ctxStreamAdd)

            menu.addSeparator()

            ActionAddFavorites = menu.addAction("Ajouter aux favoris")
            ActionAddFavorites.triggered.connect(self.ctxAddFavorites)

            ActionAddAdverts = menu.addAction("Ajouter à la liste noire")
            ActionAddAdverts.triggered.connect(self.ctxAddAdverts)

            if self.parent.player.nowplaying == "?" :
                ActionAddFavorites.setEnabled(False)
                ActionAddAdverts.setEnabled(False)

            menu.addSeparator()

            ActionAdjustCol = menu.addAction("Auto ajuster les colonnes")
            ActionAdjustCol.triggered.connect(self.ctxAdjustCol)

            ActionAdjustRow = menu.addAction("Auto ajuster les lignes")
            ActionAdjustRow.triggered.connect(self.ctxAdjustRow )

            menu.addSeparator()

            ActionStartRec = menu.addAction("Demarrer l'enregistrement")
            ActionStartRec.triggered.connect(self.ctxStartRec)

            ActionStopRec = menu.addAction("Stopper l'enregistrement")
            ActionStopRec.triggered.connect(self.ctxStopRec )

            ActionOpenDirRec = menu.addAction("Ouvrir le dossier des enregistrements")
            ActionOpenDirRec.triggered.connect(self.ctxOpenDirRec )

            menu.addSeparator()

            ActionCopyData = menu.addAction("Copier")
            ActionCopyData.triggered.connect(self.ctxCopyData )
            
        else :

            ActionAddStream = menu.addAction("Nouveau flux")
            ActionAddStream.triggered.connect(self.ctxStreamAdd)

        self.cursor_zero = event.pos()
        menu.exec_(event.globalPos())


    def ctxCopyData(self) :

        printDebug("Copier-coller -----------")
        selected = self.selectedRanges()
 
        try :
            texte = ""
            for i in range(selected[0].topRow(), selected[0].bottomRow() + 1):
                for j in range(selected[0].leftColumn(), selected[0].rightColumn() + 1):
                    try:                    
                        texte += self.item(i, j).text() + "\t"
                        printDebug("Add text cell : " + self.item(i, j).text() )
                    except AttributeError:
                        texte += "\t"

                if i != selected[0].bottomRow():
                        texte = texte[:-1] + "\n"  
                else:
                        texte = texte[:-1] 
        
            printDebug("Copier-coller  setText : " + texte )
            QApplication.clipboard().setText(texte)
        except : 
            pass


    def ctxSearchStationHome(self) :
        itemUrl = self.item(self.currentIndex().row(), configCol2Stream)
        self.streamUrl = itemUrl.text()
        stationHome = self.parent.streamList.searchStationHomeByStream( self.streamUrl  )
        if stationHome.startswith('http') :
            webbrowser.open(stationHome)
        else :
            #webbrowser.open("https://duckduckgo.com/?q="+self.parent.streamList.getNameByStream( self.streamUrl  )+"&t=hs&ia=web")
            webbrowser.open(self.settings.value("functions/configDefaultStationHome", "https://duckduckgo.com/?q")+self.parent.streamList.getNameByStream( self.streamUrl  )+"&t=hs&ia=web")


    def ctxOpenDirRec(self) :
        #subprocess.Popen(["nemo", "rip/"])
        subprocess.Popen([self.settings.value("functions/configfileExplorer", "nemo"), "rip/"])

    def ctxAdjustRow (self) :
        #self.jobStatsTable.resizeRowsToContents()
        row=0
        while row  < self.rowCount() :
            self.setRowHeight(row, int(self.settings.value("ui/configRowHeight", 30)))
            row = row + 1

    def ctxAdjustCol(self) :
        #self.resizeColumnsToContents();
        col=0
        while col  < self.columnCount() :
            printDebug("set row h = " + str(col) )
            self.setColumnWidth(col, int(self.settings.value("ui/configColWidth", 100)))
            col = col+ 1

    def ctxAddFavorites(self) :
        itemUrl = self.item(self.currentIndex().row(), configCol2Stream)
        streamUrl  = itemUrl.text()
        self.parent.streamList.addFavorite( self.parent.player.nowplaying, datetime.datetime.now(), streamUrl   )

    def ctxAddAdverts(self) :
        itemUrl = self.item(self.currentIndex().row(), configCol2Stream)
        streamUrl  = itemUrl.text()
        printDebug("Add black list = " + self.parent.player.nowplaying + " /" + streamUrl  )
        self.parent.streamList.addAdvert( self.parent.player.nowplaying, datetime.datetime.now() , streamUrl  )

    def ctxStreamPlay(self):        
        itemUrl = self.item(self.currentIndex().row(), configCol2Stream)
        self.streamUrl = itemUrl.text()
        self.streamUrlRow = self.currentIndex().row()
        self.parent.playerPlayUrl( self.streamUrl )

    def ctxProperty(self):
        itemUrl = self.item(self.currentIndex().row(), configCol2Stream)
        self.streamUrl = itemUrl.text()
        self.streamUrlRow = self.currentIndex().row()
        self.dlgProperty = dialogProperty(  self ,  self.settings , self.streamUrl )
        self.dlgProperty.show()

    def ctxStreamAdd(self):
        if self.parent.streamList.streamList is None :
            self.parent.streamList.init()

        self.dlgProperty = dialogAddNewStream(  self  )
        self.dlgProperty.show()







    def ctxStreamRemove(self):
        itemUrl = self.item(self.currentIndex().row(), configCol2Stream)
        streamUrl  = itemUrl.text()
        reply = QMessageBox.question(self, 'Confirmez', "Confirmez-vous la supression du flux : \n\n" + streamUrl  + "\n\n" + self.parent.streamList.getNameByStream( streamUrl ) , QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.parent.streamList.delByStream( streamUrl )
            self.parent.centralTableWidget.clear()
            self.parent.centralTableWidget.displayData()

    def ctxStartRec(self) :
        itemUrl = self.item(self.currentIndex().row(), configCol2Stream)
        streamUrl  = itemUrl.text()
        self.parent.streamList.startRec( streamUrl )

    def ctxStopRec(self) :
        itemUrl = self.item(self.currentIndex().row(), configCol2Stream)
        streamUrl  = itemUrl.text()
        self.parent.streamList.stopRec( streamUrl )

    def cell_was_clicked(self, row, col):
        item = self.item(row, col)
        text=""
        try:
            text = str(item.text())
            text = text[:-1]
        except:
            pass
        printDebug(text)

    def clearCol(self, col ) :
        row=0
        while row  < self.rowCount() :
            self.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            row = row + 1

    def onButtonRecSelected(self):
        printDebug("onButtonRecSelected")
        button = self.sender()
        index = self.indexAt(button.pos())
        if index.isValid():
            streamUrl = self.item(index.row(), configCol2Stream).text()
            printDebug("--- key url = "  + streamUrl)
            stateRec = self.parent.streamList.isRecByStream( streamUrl )
            if stateRec  == 1 :
                self.parent.streamList.stopRec( streamUrl )                
            else  :
                self.parent.streamList.startRec( streamUrl )                

    def onButtonPlaySelected(self):
        printDebug("---onButtonPlaySelected")
        button = self.sender()
        index = self.indexAt(button.pos())
        if index.isValid():
            streamUrl = self.item(index.row(), configCol2Stream).text()
            printDebug("--- key url = "  + streamUrl )
            state = self.parent.streamList.getStateByStream( streamUrl  )
            printDebug("state=" + state)
            if state  !=  "playing"  :
                self.parent.playerPlayUrl(  streamUrl  )
            else:
                self.parent.playerTogglePlay()
                
            printDebug("onButtonPlaySelected : @row" + str(index.row()  ) + " :" + self.streamUrl)

    #https://doc.qt.io/qt-4.8/qt.html#Key-enum
    def keyPressEvent(self, event):
        printDebug("Check keyboard key = " + str(event.key() ) + ", " + str(event.modifiers() & Qt.ControlModifier))
        self.row = self.currentIndex().row()
        self.streamUrlRow = self.currentIndex().row()
        itemUrl = self.item(self.currentIndex().row(), configCol2Stream)
        if event.key() == Qt.Key_Return or event.key() == Qt.Key_Enter :
            self.streamUrl = itemUrl.text()
            self.parent.playerPlayUrl(  self.streamUrl )
            return
        if event.key() == Qt.Key_Space :
            self.parent.playerTogglePlay()
            return
        if event.key() == Qt.Key_Plus :
            self.parent.playerVolup()
            return
        if event.key() == Qt.Key_Minus :
            self.parent.playerVoldown()
            return
        if event.key() == Qt.Key_F :            
            self.parent.streamList.addFavorite( self.parent.player.nowplaying, datetime.datetime.now() , itemUrl)
            return
        if event.key() == Qt.Key_P :            
            self.parent.streamList.addAdvert( self.parent.player.nowplaying, datetime.datetime.now() , itemUrl)
            return
        if event.key() == ( Qt.Key_Control  and Qt.Key_I ) :             
            self.importFromDir()
            return
        if event.key() == ( Qt.Key_Control  and Qt.Key_S ) :             
            self.saveAs()
            return
        if event.key() == ( Qt.Key_Control  and Qt.Key_L ) :             
            self.LoadFrom()
            return

        QtWidgets.QTableWidget.keyPressEvent(self, event)


    def saveAs( self) :
        fname = QFileDialog.getSaveFileName(self, 'Sauver les flux sous ...', '.')
        printDebug("saveAs = " + fname[0]  )
        if fname[0]  :
                dataStream = DatastreamIo()
                dataStream.writeToFile( fname[0] , self.parent.streamList)
                printDebug("saveAs done = " + fname[0] )

    def LoadFrom( self) :
        #fname = str(QFileDialog.getOpenFileName(self, 'Charger les flux', '.'))
        fname, _ = QFileDialog.getOpenFileName(self, 'Charger les flux', '.')
        printDebug("Load from  = " + fname  )
        if fname  :
                dataStream = DatastreamIo()
                dataStream.readFromFile( fname, self.parent.streamList)
                self.displayData()
                self.parent.streamList.setStateAll("?") 
                printDebug("Load from done = " + fname )

    def importFromDir( self) :
        directoryImport = str(QFileDialog.getExistingDirectory(self, "Importer les flux depuis un dossier"))
        printDebug("Import from dir : " + directoryImport)
        if directoryImport != "" :
                self.loadDataFromDir(directoryImport)
                self.displayData()

    def updateColBtnIcon( self  ) :

        iconFile = ""          

        row=0
        while row  < self.rowCount() :
            try :
                item = self.item(row, configCol2Stream)
                urlRowStream = item.text()
            except:
                itemTxt = ""
                urlRowStream=""
                pass
            rowState = self.parent.streamList.getStateByStream(urlRowStream)
            if self.parent.streamList.getLastErrorByStream(urlRowStream)  != "?"  :
                iconFile="icon/streamerror.png"
            elif rowState  == "playing" :
                iconFile="icon/play-onair.png"
            elif 	rowState  == "paused" :
                iconFile="icon/pause.png"
            else :
                iconFile="icon/play.png"

            self.updateInfo(row)

            icon = QIcon()
            icon.addPixmap(QPixmap(iconFile))
            self.cellWidget( row, configCol0Play ).setIcon(icon)

            if self.parent.streamList.isRecByStream(urlRowStream) == 1 :
                   iconFile="icon/record.png"
            else :
                   iconFile="icon/norec.png"

            iconrec = QIcon()
            iconrec.addPixmap(QPixmap(iconFile))
            self.cellWidget( row, configCol4Rec ).setIcon(iconrec)

            row = row + 1

    def initTabHeader(self ):
        self.countUrl = 0
        self.setRowCount( self.countUrl )
        self.setColumnCount(7)
        self.setHorizontalHeaderLabels(["Ecouter",  "Enr.", "Logo", "Station", "Genre","Description", "Flux"])

    def updateInfo( self  , row ):

        try :
            item = self.item(row, configCol2Stream)
            urlRowStream = item.text()
            self.item(row, configColxName).setText( self.parent.streamList.getNameByStream( urlRowStream ) );
            self.item(row, configCol5Genre).setText( self.parent.streamList.getGenreByStream( urlRowStream ) );
            self.item(row, configColxDescription).setText( self.parent.streamList.getDescriptionByStream( urlRowStream ) );
        except:
            pass

    def displayData(self ):

        self.initTabHeader()

        try :
            for streamItem  in self.parent.streamList.streamList :
                self.countUrl =  self.countUrl + 1
            self.setRowCount( self.countUrl )

            row = 0
            for streamItem  in self.parent.streamList.streamList :

                self.btn_play = QPushButton('')
                self.btn_play.clicked.connect(self.onButtonPlaySelected)
                iconplay = QIcon()
                iconplay.addPixmap(QPixmap("icon/play.png"))
                self.btn_play.setIcon(iconplay)
                self.setCellWidget(row, configCol0Play ,self.btn_play)

                self.btn_rec = QPushButton('')
                self.btn_rec.clicked.connect(self.onButtonRecSelected)
                iconrec = QIcon()
                iconrec.addPixmap(QPixmap("icon/norec.png"))
                self.btn_rec.setIcon(iconrec)
                self.setCellWidget(row, configCol4Rec ,self.btn_rec)

                self.setItem(row, configColxName, QtWidgets.QTableWidgetItem( streamItem.getName() ) )
                self.setItem(row, configColxDescription , QtWidgets.QTableWidgetItem( streamItem.getDescription() ) )
                self.setItem(row, configCol5Genre, QtWidgets.QTableWidgetItem(streamItem.getGenre()))
                self.setItem(row, configCol2Stream, QtWidgets.QTableWidgetItem( streamItem.getUrl() ))

                logo = ImgStreamLogo( self.settings, self)
                self.setCellWidget(row, configCol3Logo , logo.getLogo(  streamItem.getLogo()  ) )

                row = row + 1
        except :
            pass

        header = self.horizontalHeader()
        header.setStretchLastSection(True)

        self.updateColBtnIcon()

        row=0
        while row  < self.rowCount() :
            itemUrl = self.item(row, configCol2Stream)
            #printDebug("configCol2Stream=" + itemUrl.text())
            if itemUrl.text() == self.parent.streamList.lastAdded :
                printDebug("HERE---------=" )
                self.item(row,configCol2Stream).setBackground(QColor(125,250,1))
                self.scrollToItem(self.item(row, 0))
            row = row + 1


    def loadDataFromDir(self, dirPath ):

        if self.parent.streamList.streamList is None :
            self.parent.streamList.init()

        self.dirPath = dirPath

        dirs = os.listdir( dirPath )

        self.initTabHeader()

        self.countUrl=0
        for file in dirs:
            printDebug("file : " + dirPath + "/" + file )
            if os.path.isfile( dirPath + "/" + file )  :
                printDebug("is file ")
                if file.endswith(".m3u")  :
                     self.countUrl= self.countUrl+1

        self.setRowCount( self.rowCount() + self.countUrl )

        printDebug( "import from : " + dirPath )
        printDebug( "count = " + str( self.countUrl ) )

        #self.parent.streamList.clear()
        printDebug("Nombre de flux existants : " + str(self.parent.streamList.getCount()))

        counfFiles= 0
        for file in dirs:
            if os.path.isfile( dirPath + "/" + file )  :
                if file.endswith(".m3u")  :

                    printDebug("create line #" + str(counfFiles))

                    self.btn_sell = QPushButton('')
                    self.btn_sell.clicked.connect(self.onButtonPlaySelected)
                    icon = QIcon()
                    icon.addPixmap(QPixmap("icon/play.png"))
                    self.btn_sell.setIcon(icon)
                    self.setCellWidget(counfFiles, configCol0Play ,self.btn_sell)

                    self.btn_rec = QPushButton('')
                    self.btn_rec.clicked.connect(self.onButtonRecSelected)
                    iconrec = QIcon()
                    iconrec.addPixmap(QPixmap("icon/norec.png"))
                    self.btn_rec.setIcon(iconrec)
                    self.setCellWidget(counfFiles, configCol4Rec ,self.btn_rec)

                    fp = open(dirPath + "/" +  file, 'r', encoding='ISO-8859-1')
                    file_lines = fp.readlines()
                    fp.close()
                    for lineUrl in file_lines:
                        if lineUrl.startswith('http:') :
                            lineUrl= lineUrl.replace("\n","")

                            self.setItem(counfFiles, configCol2Stream, QtWidgets.QTableWidgetItem(lineUrl))

                            logo = ImgStreamLogo( self.settings, self)
                            self.setCellWidget(counfFiles, configCol3Logo , logo.getLogo( dirPath + "/" + file))
                            self.parent.streamList.addStream( lineUrl )

                            break

                    self.setItem(counfFiles, configColxName, QtWidgets.QTableWidgetItem("[nouv. flux]"))
                    self.parent.streamList.setNameByStream( lineUrl, "[nouv. flux]" )

                    self.setItem(counfFiles, configCol5Genre, QtWidgets.QTableWidgetItem(""))
                    self.parent.streamList.setGenreByStream( lineUrl, "" )

                    self.setItem(counfFiles, configColxDescription, QtWidgets.QTableWidgetItem(""))
                    self.parent.streamList.setDescriptionByStream( lineUrl, "" )

                    counfFiles=counfFiles+1

        self.resizeColumnsToContents();
        header = self.horizontalHeader()
        header.setStretchLastSection(True)

        self.updateColBtnIcon()

class MyMainWindow(QMainWindow):

    def __init__(self ):
        super().__init__()

        self.readSettings()

        self.player = MyPlayer(self, self.settings )
        self.streamList = MyStreams(self )
        self.streamList.setSettings( self.settings )
        self.centralTableWidget = MyTableWidget( self, self.settings)

        self.initUI()

        #self.streamList.addStream( "lineUrl" )
        #self.streamList.addStream("self.editStreamAdress.text().strip()" )
        #self.centralTableWidget.displayData()

        self.playerLoad()



    def initUI(self):

        self.setWindowIcon(QIcon('icon/app.png'))

        self.centralTableWidget.initTabHeader();

        self.nowplayingWidget  = QLabel('nowPlaying', self)
        centralWidget = QWidget()
        gridLayout=QGridLayout()
        gridLayout.addWidget(self.centralTableWidget,1,0)
        gridLayout.addWidget(self.nowplayingWidget ,0,0)
        self.setCentralWidget(centralWidget )
        centralWidget.setLayout(gridLayout)

        ritchText = ""
        self.nowplayingWidget.setText(ritchText)

        self.statusBar().showMessage('Ready : ' + str(self.centralTableWidget.countUrl) + ' flux chargés.')

        self.toolbar  = QToolBar(self)
        self.addToolBar(Qt.TopToolBarArea, self.toolbar )
        #self.toolbar = self.addToolBar('toolbar',  Qt.LeftToolBarArea)
        self.toolbar.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.playerPauseAct = QAction(QIcon('icon/play.png'), 'Ecouter/Pause', self)
        self.playerPauseAct.setStatusTip(self.tr("Ecouter/Pause"))
        self.playerPauseAct.triggered.connect( self.playerTogglePlay )
        self.toolbar.addAction(self.playerPauseAct)

        self.playerVoldownAct = QAction(QIcon('icon/voldown.png'), 'Volume -', self)
        self.playerVoldownAct.setStatusTip(self.tr("Diminuer le volume"))
        self.playerVoldownAct.triggered.connect( self.playerVoldown )
        self.toolbar.addAction(self.playerVoldownAct)

        self.playerVolupAct = QAction(QIcon('icon/volup.png'), 'Volume +', self)
        self.playerVolupAct.setStatusTip(self.tr("Augmenter le volume"))
        self.playerVolupAct.triggered.connect( self.playerVolup )
        self.toolbar.addAction(self.playerVolupAct)

        self.playerShutdownAct = QAction(QIcon('icon/shutdown.png'), 'Fermer', self)
        self.playerShutdownAct.setStatusTip(self.tr("Fermer Vlc"))
        self.playerShutdownAct.triggered.connect( self.playerShutdown )
        self.toolbar.addAction(self.playerShutdownAct)

        self.playerResetAct = QAction(QIcon('icon/reset.png'), 'Reset Vlc', self)
        self.playerResetAct.setStatusTip(self.tr("Remise à z&ro de la configuration Vlc"))
        self.playerResetAct.triggered.connect( self.playerReset )
        self.toolbar.addAction(self.playerResetAct)

        """
        self.playerSaveAct = QAction(QIcon('icon/save.png'), 'Sauver', self)
        self.playerSaveAct.setStatusTip(self.tr("Sauvegarder les flux"))
        self.playerSaveAct.triggered.connect( self.playerSave )
        self.toolbar.addAction(self.playerSaveAct)

        self.playerLoadAct = QAction(QIcon('icon/load.png'), 'Charger', self)
        self.playerLoadAct.setStatusTip(self.tr("Charger les flux"))
        self.playerLoadAct.triggered.connect( self.playerLoad )
        self.toolbar.addAction(self.playerLoadAct)

        self.playerImportFromDirAct = QAction(QIcon('icon/import.png'), 'Import', self)
        self.playerImportFromDirAct.setStatusTip(self.tr("Import"))
        self.playerImportFromDirAct.triggered.connect( self.playerImportFromDir)
        self.toolbar.addAction(self.playerImportFromDirAct)
        """

        self.FavoritesAct = QAction(QIcon('icon/favorites.png'), 'Favoris', self)
        self.FavoritesAct.setStatusTip(self.tr("Liste des pistes préférées"))
        self.FavoritesAct.triggered.connect( self.showFavorites)
        self.toolbar.addAction(self.FavoritesAct)

        self.AddAdvertAct = QAction(QIcon('icon/adds.png'), 'Liste noire', self)
        self.AddAdvertAct.setStatusTip(self.tr("Liste noire"))
        self.AddAdvertAct.triggered.connect( self.showAdds)
        self.toolbar.addAction(self.AddAdvertAct)

        self.aboutAct = QAction(QIcon('icon/about.png'), 'A propos', self)
        self.aboutAct.setStatusTip(self.tr("A propos"))
        self.aboutAct.triggered.connect( self.about )
        self.toolbar.addAction(self.aboutAct)

        self.exitAction = QAction(QIcon('icon/exit.png'), 'Fermer', self)
        self.exitAction.setStatusTip(self.tr("Fermer le poste"))
        self.exitAction.setShortcut('Ctrl+Q')
        self.exitAction.triggered.connect( self.close )
        self.toolbar.addAction(self.exitAction)

        self.toolbar.setIconSize(QSize(int(self.settings.value("ui/configToolbarIconSize", 48)), int(self.settings.value("ui/configToolbarIconSize", 48))))       

        self.resize(self.settings.value('winsize', QSize(270, 225)))
        self.move(self.settings.value('winpos', QPoint(50, 50)))

        self.panelDisplay()

        self.show()

        self.initWindowTitle()

    def onChangeNowPlaying(self) : 
        printDebug(" *** onChangeNowPlaying  : " + self.player.nowplaying  + " **********")
        if self.player.isPlayingAdv( ) == 1:
            #printDebug( "Conf Audiovolume is =  " + str(self.player.audiovolume))
            #printDebug( "Conf audioVolumeBeforeAd is =  " + str(self.player.audioVolumeBeforeAd))
            #printDebug( "Conf Adv volume config = " + str( int(self.settings.value("streams/configVolumeAd", 10)) ) )            
            #self.player.volAdv()
            #printDebug( "Volume set = " + str( int(self.settings.value("streams/configVolumeAd", 10)) ) )            
            self.panelDisplay()
        else :
            #printDebug( "VOLUME back Audiovolume is : " + str(self.player.audiovolume))
            #printDebug( "VOLUME back audioVolumeBeforeAd : " + str(self.player.audioVolumeBeforeAd))
            self.panelDisplay()

        #self.panelDisplay()

    def initWindowTitle(self) :
        self.setWindowTitle('Flux radios')

    def panelDisplay(self):

        self.centralTableWidget.updateColBtnIcon( )

        if len(self.player.lasterror) > 1 :
            ritchText = "<html><head>/<meta name=\"qrichtext\" content=\"1\" /></head>" \
                      "<body style=\"font-family:Sans Serif; " \
                      "<p style=\" margin-top:0px; \"><font-size:18pt><font-color=\"red\"><b>" + self.player.lasterror  + ", url=" + self.player.url + "</b></font></font></p>" \
                      "</body></html>"
            self.nowplayingWidget.setText(ritchText)
            self.setWindowTitle( "Erreur de lecture du flux")
        else :
            if self.player.isPlayingAdv() :
                playingAdv = "[BLACK LIST]"
                #self.player.audiovolume = self.player.audioVolumeBeforeAd 
                printDebug( "BLACK LIST - VOLUME Audiovolume is : " + str(self.player.audiovolume))
                printDebug( "BLACK LIST - VOLUME audioVolumeBeforeAd : " + str(self.player.audioVolumeBeforeAd))                
                #self.player.vol( 9 )
            else :
                playingAdv = ""
                #self.player.audiovolume = self.player.audioVolumeBeforeAd                

                printDebug( "VOLUME Audiovolume is : " + str(self.player.audiovolume))
                printDebug( "VOLUME audioVolumeBeforeAd : " + str(self.player.audioVolumeBeforeAd))                
                #self.player.vol( self.player.audioVolumeBeforeAd )
                #self.player.audioVolumeBeforeAd = self.player.audiovolume          
                #if self.player.audioVolumeBeforeAd == "0" :
                #    self.player.audioVolumeBeforeAd = 95
                    				
            if  self.player.url != "?" :
                ritchText = "<html><head><meta name=\"qrichtext\" content=\"1\" /></head>"\
                   "<body style=\" white-space: pre-wrap; font-family:Sans Serif; "\
                   "font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">"\
                   "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; "\
                   "margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-color=#102030 \"> "\
                    + playingAdv + " " + self.player.state + " : <b>" + self.player.nowplaying  + "</b></font> (Station : <i>" + self.player.icy_name +")</i> \
             </p>" "</body></html>"
                self.nowplayingWidget.setText(ritchText)
                self.nowplayingWidget.setWindowOpacity(0.5)
                self.setWindowTitle( playingAdv + " " + self.player.state+ ":" + self.player.nowplaying  + " (Vol=" +  str(self.player.audiovolume) + ")")

                if self.player.state == "paused" :
                   self.playerPauseAct.setIcon(QIcon('icon/pause.png'))
                elif self.player.state == "playing" :
                   self.playerPauseAct.setIcon(QIcon('icon/play-onair.png'))
                else :
                   self.playerPauseAct.setIcon(QIcon('icon/play.png'))

            else:
                self.initWindowTitle( )
                self.nowplayingWidget.setText("")

    #def unfadePanelDisplay(self):
         #self.nowplayingWidget.setWindowOpacity(1)
         #printDebug("unfade")

    def playerTogglePlay(self):
        self.player.togglePause()
        QTimer.singleShot(int(self.settings.value("streams/configGrabinfoDelay", 300)), self.player.grabInfos)

    def playerPlayUrl(self, url):
        self.player.shutdownForce()
        self.player.playUrl( url )
        QTimer.singleShot(int(self.settings.value("streams/configGrabinfoDelay", 300)), self.player.grabInfos)

    def playerVoldown(self):
        self.player.voldown()
        QTimer.singleShot(int(self.settings.value("streams/configGrabinfoDelay", 300)), self.player.grabInfos)

    def playerVolup(self):
        self.player.volup()
        QTimer.singleShot(int(self.settings.value("streams/configGrabinfoDelay", 300)), self.player.grabInfos)

    def playerShutdown(self):
        self.player.shutdown()
        QTimer.singleShot(int(self.settings.value("streams/configGrabinfoDelay", 300)), self.player.grabInfos)
        self.player.resetInfo()
        self.streamList.initStreamList()
        self.centralTableWidget.updateColBtnIcon()
        self.panelDisplay()

    def playerReset(self):
        self.player.reset()
        QTimer.singleShot(int(self.settings.value("streams/configGrabinfoDelay", 300)), self.player.grabInfos)
        self.centralTableWidget.updateColBtnIcon()

    def playerLoad(self):
         dataStream = DatastreamIo()
         self.streamList.clear()
         self.centralTableWidget.clear()
         dataStream.readFromFile( self.settings.value("functions/configDatabaseFileName", "pyradio.bdd"), self.streamList)
         self.centralTableWidget.displayData()
         self.streamList.setStateAll("?") 

         self.centralTableWidget.horizontalHeader().restoreState( self.settings.value("mainTableState") )

         colSort = self.centralTableWidget.horizontalHeader().sortIndicatorSection()
         colOrder = self.centralTableWidget.horizontalHeader().sortIndicatorOrder()
         self.centralTableWidget.sortItems(colSort , colOrder)
         self.centralTableWidget.setSortingEnabled(False)

    def playerSave(self):
         dataStream = DatastreamIo()
         dataStream.writeToFile( self.settings.value("functions/configDatabaseFileName", "pyradio.bdd"), self.streamList)

    def playerRecord(self):
         self.player.record()

    def playerImportFromDir(self):
         self.streamList.clear()
         self.centralTableWidget.loadDataFromDir("/home/laurent/mount/tchome/Documents/Perso/pc - dev/pyRadio/import/")

    def showFavorites(self) :
        self.dlgFavorites = dialogFavorite(  self  )
        self.dlgFavorites.show()

    def showAdds(self) :
        self.dlgAdverts = dialogAdverts(  self  )
        self.dlgAdverts.show()

    def about(self):
        QMessageBox.about(self, self.tr("A propos"),
            self.tr("<b>Gestionnaire de flux audios</b><br>" +
                    "Gui pour Vlc et StreamRipper<br><br>" +
                    "v0.1b<br>" +
                    "dec. 2016<br>"  +
                    "<br>"  +
                    "<b>[Espace]</b> : Pause / Jouer<br>" +
                    "<b>[Entrée]</b> : Jouer<br>" +
                    "<b>[+] [-]</b>  : Volume + / Volume -<br>"+
                    "<b>[P]</b> : Mettre la piste en cours sur la liste noire<br>" +
                    "<b>[F]</b> : Mettre la piste en cours sur la des favoris<br>" +
                    "<b>[Ctrl]+[S]</b> : Sauver les flux sous ...<br>" +
                    "<b>[Ctrl]+[L]</b> : Charger les flux depuis ...<br>" +
                    "<b>[Ctrl]+[I]</b> : Importer depuis les fichiers *.m3u<br>" + 
                    "<b>[Ctrl]+[A]</b> + Clic-droit+ Copier : Copier les données du tableau des flux<br>" 
		    ) )

    def closeEvent(self, event):

        reply = QMessageBox.question(self, 'Confirmez',
            "Fermer le programme ?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            self.player.shutdownForce()
            self.playerSave()
            self.storeSettings()
            event.accept()
        else:
            event.ignore()

    def storeSettings(self):
        self.settingsWrite = QSettings("conf.ini", QSettings.IniFormat)
        self.settingsWrite.setFallbacksEnabled(False)    

        self.settingsWrite.setValue("winsize", self.size())
        self.settingsWrite.setValue("winpos", self.pos())
        header = self.centralTableWidget.horizontalHeader()
        self.settingsWrite.setValue("mainTableState", header.saveState())

        self.settingsWrite.setValue("streams/configGrabinfoDelay",  self.settings.value("streams/configGrabinfoDelay", 300))
        self.settingsWrite.setValue("streams/configMaxNowPlayingHistory",  self.settings.value("streams/configMaxNowPlayingHistory", 16))
        self.settingsWrite.setValue("streams/configMaxPubHistory",  self.settings.value("streams/configMaxPubHistory", 100))
        self.settingsWrite.setValue("streams/configMaxFavoriteHistory",  self.settings.value("streams/configMaxFavoriteHistory", 999))
        self.settingsWrite.setValue("streams/configVolumeAd",  self.settings.value("streams/configVolumeAd", 10))

        self.settingsWrite.setValue("ui/configFontFamilly",  self.settings.value("ui/configFontFamilly", "Arial"))
        self.settingsWrite.setValue("ui/configFontSize",  self.settings.value("ui/configFontSize", 12))
        self.settingsWrite.setValue("ui/configLogoH",  self.settings.value("ui/configLogoH", 64))
        self.settingsWrite.setValue("ui/configLogoW",  self.settings.value("ui/configLogoW", 64))
        self.settingsWrite.setValue("ui/configTrackDateFormat",  self.settings.value("ui/configTrackDateFormat", "%d-%m-%Y %H:%M"))
        self.settingsWrite.setValue("ui/configToolbarIconSize",  self.settings.value("ui/configToolbarIconSize", 48))
        self.settingsWrite.setValue("ui/configRowHeight",  self.settings.value("ui/configRowHeight", 30))
        self.settingsWrite.setValue("ui/configColWidth",  self.settings.value("ui/configColWidth", 100))

        self.settingsWrite.setValue("functions/configSearchWeb",  self.settings.value("functions/configSearchWeb", "http://pleer.net/search?q="))
        self.settingsWrite.setValue("functions/configfileExplorer",  self.settings.value("functions/configfileExplorer", "nemo"))
        self.settingsWrite.setValue("functions/configDatabaseFileName",  self.settings.value("functions/configDatabaseFileName", "pyradio.bdd"))
        self.settingsWrite.setValue("functions/configDefaultStationHome",  self.settings.value("functions/configDefaultStationHome", "https://duckduckgo.com/?q="))

        self.settingsWrite.setValue("debug/configDebug",  self.settings.value("debug/configDebug", 0))
        self.settingsWrite.setValue("debug/configDebugToFile",  self.settings.value("debug/configDebugToFile", 0))


    def readSettings(self):
        global configTrackDateFormat
        global debugMode

        self.settings = QSettings("conf.ini", QSettings.IniFormat)
        self.settings.setFallbacksEnabled(False)    
        
        debugMode = self.settings.value("debug/configDebug", 0)
        #configTrackDateFormat = self.settings.value("ui/configTrackDateFormat", "%d-%m-%Y %H:%M")
        #print( "debugMode  = " + str(debugMode) )
        #printDebug( "printdebug debugMode  = " + str(debugMode) )
        #print( "configGrabinfoDelay = " + self.settings.value("streams/configGrabinfoDelay", 300) )
        #print( "configTrackDateFormat  = " + configTrackDateFormat )


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = MyMainWindow(  )
    sys.exit(app.exec_())

