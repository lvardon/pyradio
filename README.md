# pyRadio #

Script à  l'![a.r.a.c.h.e](http://www.la-rache.com/) de gestion de liste de flux radios Web.
Gui pour Vlc media player et Streamripper.

Prérequis :
 * Os : Linux (non testé pour Ms Win.)
 * Python
 * Vlc  média player : au minimum
 * Streamripper : optionnel  ![streamripper home page](http://streamripper.sourceforge.net/) 

![app appscreenshoot](icon/appscreenshoot.png)

GPL : Feel free to fork !

## Installation ##

 Dézipper et lancer *fluxmanager.py* :
 
 * Optionnel : ajuster les paramètres de *config.ini*
 * Executer la commande : *python fluxmanager.py*
 * Ou bien : Clic-droit -> ouvrir *fluxmanager.py*
  
## 15/01/2017 , version initiale ##

 * Version stable et fonctionelle pour la lecture et l'enregistrement.
 * nb : éviter d'utiliser la "liste noire" (qui permet d'ajuster le volume en fonction du titre). Cf Fix.

## Fix To Do ##

 * Liste noire. Actuellement non fonctionelle  : elle n'ajuste pas correctement le niveau sonnore.

## To do ##

 * Documentation
 * Pouvoir exporter les pistes au format mp3u
 * Pouvoir modifier les propriétés de chaque station

